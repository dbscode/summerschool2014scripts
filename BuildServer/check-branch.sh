#!/bin/sh

cd ~/Documents/SourceCode/

git fetch $2

SHAA="$(git rev-parse $1)"
SHAB="$(git rev-parse $2/$1)"

#echo "A:$SHAA"
#echo "B:$SHAB"

if [ $SHAA = $SHAB ]; then
	echo "Branch $1 has not been updated"
	exit 0
else
	echo "Branch $1 has been updated"
	git checkout $1
	git reset --hard
	git clean -f -x -d
	git merge $2/$1
	exit 1
fi
