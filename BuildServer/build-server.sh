#!/bin/sh

cd "`dirname \"$0\"`"
SERVERPATH=$PWD

# disable the screen saver
xset s off
xset -dpms
xset s noblank

# Setup the audio
amixer cset numid=3 1

while true
do
	./check-branch.sh master origin
	if [ $? != 0 ]; then
		./run-tests.sh
		
		if [ $? != 0 ]; then
			echo "failed" > ~/Documents/BuildResults/status.txt
			echo "Red"
			aplay "$PWD/alert.wav"
			aplay "$PWD/alert.wav"
			aplay "$PWD/alert.wav"
		else
			echo "ok" > ~/Documents/BuildResults/status.txt
			echo "Green"
		fi
	fi

	sleep 1m
done
