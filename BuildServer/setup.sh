#!/bin/sh

cd "`dirname \"$0\"`"
DIRNAME=$PWD

../InstallScripts/install-apache.sh
sudo apt-get install x11-xserver-utils

sudo cp $DIRNAME/www/* /var/www/

cd ~/Documents
mkdir BuildResults
cd BuildResults
echo "building" > status.txt
