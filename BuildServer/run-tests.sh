#!/bin/sh

cd ~/Documents/SourceCode/

echo "Building"
./buildserver.sh > build-log.txt 2>&1
STATUS=$?

sudo mv build-log.txt /var/www/

exit $STATUS
