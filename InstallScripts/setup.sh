#!/bin/sh

mkdir -p ~/Documents

./update.sh
./install-geany.sh
./install-git-gui.sh
./install-apache.sh
./desktop-shortcuts.sh
./download-phpunit.sh
./clone-git-repository.sh
