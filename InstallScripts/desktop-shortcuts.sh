#!/bin/sh

DESKTOP_PATH=~/Desktop
UNUSED_NAME="UnusedShortcuts"

if [ ! -d "$DESKTOP_PATH/$UNUSED_NAME" ]; then
	echo "Creating Directory $DESKTOP_PATH/$UNUSED_NAME"
	mkdir "$DESKTOP_PATH/$UNUSED_NAME"
fi

mv $DESKTOP_PATH/*.desktop $DESKTOP_PATH/$UNUSED_NAME/
cp desktop-shortcuts/*.desktop $DESKTOP_PATH/
